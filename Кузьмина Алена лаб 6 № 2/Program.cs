﻿using System;

namespace Кузьмина_Алена_лаб_6___2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите размер матрицы");
            int n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Исходная матрица:");
            int[,] R = new int[n, n];
            var random = new Random();

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    R[i, j] = random.Next(-10, 100);
                    Console.Write(R[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine("Полученная матрица:");

            for (int i = 0; i < n; i++)
            {
                int minInStr = R[i, 0];
                for (int j = 0; j < n; j++)
                {
                    if (R[i, j] <= minInStr)
                    {
                        minInStr = R[i, j];
                    }
                    if (j == n - 1)
                        R[i, 0] = minInStr;
                }
            }
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Console.Write(R[i, j] + " ");
                }
                Console.WriteLine();
            }
        }
    }
}
