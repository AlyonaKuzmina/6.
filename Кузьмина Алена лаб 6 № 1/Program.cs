﻿using System;

namespace Кузьмина_Алена_лаб_6___1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите количество элементов в массиве:");
            int n = Convert.ToInt32(Console.ReadLine());
            int[] Z = new int[n];
            int S = 0;

            Console.WriteLine("Введите границы массива а и b");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            Random r = new Random();
            Console.Write("Массив:");
            Console.WriteLine();

            for (var i = 0; i < Z.Length; i++)
            {
                Z[i] = r.Next(a, b);
                Console.Write(Z[i] + " ");
            }
            Console.WriteLine();

            for (var i = 0; i < Z.Length; i++)
                if (((Z[i] % 2) == 0) && (Z[i] < 3))
                    S += Z[i];
            Console.WriteLine("S= " + S);

            if (b > 3)
            {
                int P = 1;
                for (var i = 0; i < Z.Length; i++)
                    if (((Z[i] % 2) != 0) && (Z[i] > 1))
                        P *= Z[i];
                Console.WriteLine("P= " + P);
                int R = S + P;
                Console.WriteLine("R= " + R);
            }
            else if (b <= 3)
            {
                int P = 0;
                Console.WriteLine("P= " + P);

                int R = S + P;
                Console.WriteLine("R= " + R);
            }
        }
    }
}
